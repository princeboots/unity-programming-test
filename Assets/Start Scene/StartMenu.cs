﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartMenu : MonoBehaviour 
{

	void Update() 
    {
		if (Input.GetKeyDown(KeyCode.Return) || Input.touchCount > 0)
		{
			SceneManager.LoadScene(1);
		}

	}
	
}
