﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

	public bool hasLaunched = false;
	public bool outOfBalls = false;
	public int breakableBlocks = 0;
	public GameObject WinPanel, LosePanel;

	private void Update()
	{

		if (breakableBlocks == 0 && hasLaunched )
		{
			WinPanel.SetActive(true);
			if (Input.GetKeyDown(KeyCode.Space))
			{
				ResetScene();
			}
		}
		else if (outOfBalls)
		{
			LosePanel.SetActive(true);
			if (Input.GetKeyDown(KeyCode.Space))
			{
				ResetScene();
			}
		}
	}

	private void ResetScene()
	{
		breakableBlocks = 0;
		hasLaunched = false;
		WinPanel.SetActive(false);
		LosePanel.SetActive(false);
		SceneManager.LoadScene(1);
	}

}
