﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour 
{
	public int ballsLeft = 5;								// how many balls left
	public bool preLaunch = true;						// is the ball waiting to be launched?

	[SerializeField]	private float launchDelay;		// the time it takes to launch the ball
	[SerializeField]	private float launchTimer;		// keeps track of time until launch
	[SerializeField]	private Ball ball;              // the ball
	[SerializeField]	private GameManager gameManager;


	private void Update()
	{
		// waits until enough time has passed then launches the ball
		if (preLaunch)
		{
			if (launchTimer > launchDelay)
			{
				preLaunch = false;
				ball.Launch();
			}
			launchTimer += Time.deltaTime;
		}
	}

	public void LoseBall()
	{
		ballsLeft--;
		if (ballsLeft < 0)
		{
			gameManager.outOfBalls = true;
			return;
		}
		preLaunch = true;
		launchTimer = 0f;
	}

}
