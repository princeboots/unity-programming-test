﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour, IBreakable
{

	[SerializeField]	private bool isBreakable = true;
	[SerializeField]	private GameObject smoke;
	[SerializeField]	private GameManager gameManager;

	private AudioSource audioSource;

	private void Start()
	{
		if (isBreakable) gameManager.breakableBlocks++;
		audioSource = GetComponent<AudioSource>();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		// ignore the secondary ball until it has been activated by paddle touch
		if (collision.collider.tag == "Inactive") return;

		audioSource.Play();
		if (isBreakable)
		{
			Break();
		}
	}

	public void Break()
	{
		gameManager.breakableBlocks--;
		GameObject breakParticles = Instantiate(smoke, this.transform.position, Quaternion.identity);
		Destroy(breakParticles, 1f);

		//disable the sprite and collider then destroy after the sound has finished - would rework if time allowed
		GetComponent<SpriteRenderer>().enabled = false;
		GetComponent<Collider2D>().enabled = false;
		Destroy(gameObject, 0.2f);
	}

}
