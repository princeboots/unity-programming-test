﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BallsLeft : MonoBehaviour 
{
	[SerializeField] BallManager ballManager;

	private Text UItext;

	private void Start()
	{
		UItext = GetComponent<Text>();
	}

	private void Update()
	{
		UItext.text = "Balls Left: " + ballManager.ballsLeft.ToString();
	}

}
