﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// bonus ball which inherits from ball, but won't destroy blocks until activated
// ignores launch points and won't trigger 'lose ball'
public class BonusBall : Ball
{

	protected override void Start()
	{
		// Inactive tag prevents blocks from breaking
		tag = "Inactive";
		rigidBody = GetComponent<Rigidbody2D>();
		rigidBody.velocity = new Vector2( 0.7f * moveSpeed, -0.7f * moveSpeed );
	}

	protected override void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "LoseBall")
		{
			Destroy(gameObject);
		}
	}

	protected override void OnCollisionEnter2D(Collision2D coll)
	{
		// can break blocks once the paddle has touched it, tagged as another Ball for extendability
		if (coll.collider.tag == "Paddle")
		{
			tag = "Ball";
		}
		base.OnCollisionEnter2D(coll);

	}


}
