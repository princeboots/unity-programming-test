﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchPoint : MonoBehaviour 
{
	public Vector3 launchDirection;							// the launch direction to be passed to the ball on launch

	[SerializeField] private Transform launchTarget;		// the child transform that the ball will be launched toward

	void Awake() 
    {
		launchTarget = transform.GetChild(0);
		launchDirection = launchTarget.transform.position - transform.position;
	}

	// used in Editor to visualize launch points and directions
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(transform.position, 0.3f);
		Gizmos.DrawLine(transform.position, launchTarget.transform.position);
	}

}
