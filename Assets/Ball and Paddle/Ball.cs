﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour 
{
	public Vector3 launchDirection;									// direction the ball will launch (determined by launch points)

	[SerializeField]	private List<LaunchPoint> allLaunchPoints;	// list of all launch points in scene
	[SerializeField]	private LaunchPoint launchPoint;			// object that keeps position and direction launch
	[SerializeField]	private BallManager ballManager;            // the Ball Manager
	[SerializeField]	private GameManager gameManager;            // the Game Manager

	[SerializeField]	protected float moveSpeed;					// how fast the ball moves

	protected Rigidbody2D rigidBody;

	protected virtual void Start()
	{
		rigidBody = GetComponent<Rigidbody2D>();
		transform.position = launchPoint.transform.position;
	}

	public void Launch()
	{
		//direction and position determined by launchpoint
		transform.position = launchPoint.transform.position;
		launchDirection = launchPoint.launchDirection;

		// normalize the direction vector and then apply the appropriate speed o the ball
		launchDirection.Normalize();
		rigidBody.velocity = moveSpeed * launchDirection;
		gameManager.hasLaunched = true;
	}

	protected virtual void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "LoseBall" && gameManager.breakableBlocks > 0 )
		{
			// choose random launch point for next ball and place the ball there
			int n = Random.Range(0, allLaunchPoints.Count);
			launchPoint = allLaunchPoints[n];
			transform.position = launchPoint.transform.position;
			rigidBody.velocity = Vector3.zero;

			// call LoseBall method from our Ball Manager (which will initiate the next launch)
			ballManager.LoseBall();
		}
	}

	protected virtual void OnCollisionEnter2D (Collision2D coll) 
    {
		// cache the current speed of the ball if it has increased speeds due to collisions after launch
		if (rigidBody.velocity.magnitude > moveSpeed) moveSpeed = rigidBody.velocity.magnitude;


		if (coll.collider.tag == "Paddle")
		{
			// the collision offset is the distance from paddle centre at which the ball hit the paddle
			float collisionOffset = (coll.contacts[0].point.x - coll.transform.position.x);
			//Debug.Log("Ball hit paddle :" + (collisionOffset) + " units from paddle centre");

			// we clamp the value to prevent the new bounce from taking too sharp an angle
			collisionOffset = Mathf.Clamp(collisionOffset, -0.7f, 0.7f);

			// now we find the new bounce direction determined by the offset
			Vector2 newDirection = new Vector3(collisionOffset, 1 - Mathf.Abs(collisionOffset), 0);

			// normalize and multiply by moveSpeed to preserve speed
			newDirection.Normalize();
			rigidBody.velocity = newDirection * moveSpeed;
		}

		// add a small tweak to prevent ball from getting stuck with no Y velocity
		Vector2 tweak = new Vector2( 0, Random.Range(-0.1f, 0.2f));
		rigidBody.velocity += tweak;
	}
	
}
