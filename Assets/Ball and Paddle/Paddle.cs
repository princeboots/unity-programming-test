﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour 
{

	[SerializeField]	private float speed;				//speed at which the paddle can move
	[SerializeField]	private float paddleWidth = 1f;     //distance from the center of the paddle to its end
	[SerializeField]	private float playSpaceWidth;		// width from center (in world units) of play space
	 
	void Start() 
    {
		playSpaceWidth = (float)Screen.width/Screen.height * 5f;
	}

	private void Update()
	{
		HandleMovement();
	}

	private void HandleMovement()
	{
		float horizontal = Input.GetAxis("Horizontal");

		// if paddle is touching right wall and player is moving right, do nothing
		if (horizontal > 0 && ( transform.position.x > playSpaceWidth - paddleWidth ))
		{
			return;
		}

		// if paddle is touching left wall and player is moving left, do nothing
		if (horizontal < 0 && ( transform.position.x < paddleWidth - playSpaceWidth ))
		{
			return;
		}

		// otherwise move the desired direction
		transform.position += new Vector3(speed * horizontal * Time.deltaTime, 0, 0);
	}

}
